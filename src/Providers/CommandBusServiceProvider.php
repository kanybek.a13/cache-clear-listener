<?php

declare(strict_types=1);

namespace Akgramm\ClearCache\Providers;

use Akgramm\ClearCache\Commands\ClearCacheCommand;
use Akgramm\ClearCache\Handlers\ClearCacheHandler;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\ServiceProvider;

class CommandBusServiceProvider extends ServiceProvider
{
    private array $maps = [
        ClearCacheCommand::class  => ClearCacheHandler::class,
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCommandHandlers();
    }

    private function registerCommandHandlers()
    {
        Bus::map($this->maps);
    }
}
