<?php

declare(strict_types=1);

namespace Akgramm\ClearCache\Listeners;

use Akgramm\ClearCache\Commands\ClearCacheCommand;

final class CacheClearListener
{
    public function handle($event): void
    {
        dispatch(new ClearCacheCommand());
    }
}
