<?php

declare(strict_types=1);

namespace Akgramm\ClearCache;

use Illuminate\Support\ServiceProvider;
use Akgramm\ClearCache\Providers\CommandBusServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->register(CommandBusServiceProvider::class);
    }
}
