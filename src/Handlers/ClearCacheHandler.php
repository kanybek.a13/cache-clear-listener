<?php

declare(strict_types=1);

namespace Akgramm\ClearCache\Handlers;

use Illuminate\Support\Facades\Artisan;

final class ClearCacheHandler
{
    public function handle(): void
    {
        Artisan::call('optimize:clear');
    }
}
